package
{
	import flash.geom.Point;

	public class JigsawPuzzleInfo
	{
		private var _name:String;
		private var _widthInPixels:uint;
		private var _heightInPixels:uint;
		private var _imageURLs:Vector.<String>;
		private var _points:Vector.<Point>;
		private var _connections:Array;
		
		public function JigsawPuzzleInfo(Target:Object)
		{
			trace("JigsawPuzzle(Target:" + Target + ")");
			
			var SuccessfulInd:Boolean = true;
			// Does the object have the correct properties?
			var PropertiesList:Array = ["name", "width", "height", "pieces", "connections"];
			while (PropertiesList.length > 0)
			{
				var Property:String = PropertiesList.pop();
				if (!Target.hasOwnProperty(Property))
					SuccessfulInd = false;
			}
			
			_name = Target.name;
			_widthInPixels = Target.width;
			_heightInPixels = Target.height;
			
			// Fill in puzzle piece coordinates and image URLs
			_points = new Vector.<Point>();
			_imageURLs = new Vector.<String>();
			for each (var PieceObj:Object in Target.pieces)
			{
				if (PieceObj.hasOwnProperty("x") && 
					PieceObj.hasOwnProperty("y"))
					_points.push(new Point(-PieceObj.x, -PieceObj.y));
				else
					SuccessfulInd = false;
				
				if (PieceObj.hasOwnProperty("file_location"))
					_imageURLs.push(PieceObj.file_location);
				else
					SuccessfulInd = false;
			}
			_connections = new Array();
			for each (var ConnectionObj:Object in Target.connections)
			{
				if (ConnectionObj.hasOwnProperty("first_index") && 
					ConnectionObj.hasOwnProperty("second_index"))
				{
					var FirstIndex:uint = ConnectionObj.first_index;
					var SecondIndex:uint = ConnectionObj.second_index;
					if (_connections[FirstIndex] == null)
						_connections[FirstIndex] = new Array();
					(_connections[FirstIndex] as Array).push(SecondIndex);
					if (_connections[SecondIndex] == null)
						_connections[SecondIndex] = new Array();
					(_connections[SecondIndex] as Array).push(FirstIndex);
				}
				else
					SuccessfulInd = false;
			}
			
			if (!SuccessfulInd)
				throw new Error("JigsawPuzzle not successfully created!");
		}
		
		public function get imageURLs():Vector.<String>
		{
			return _imageURLs;
		}
		
		public function get points():Vector.<Point>
		{
			return _points;
		}
		
		public function get connections():Array
		{
			return _connections;
		}
	}
}