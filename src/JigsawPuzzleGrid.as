package
{
	import flash.geom.Point;

	public class JigsawPuzzleGrid
	{
		private var _widthInPieces:uint = 0;
		private var _heightInPieces:uint = 0;
		private var _pieceWidth:uint = 48;
		private var _pieceHeight:uint = 48;
		private var _jitterPercent:Point;
		
		private var _jitters:Vector.<Point>;
		private var _currentPoint:Point;
		
		public function JigsawPuzzleGrid(WidthInPieces:uint, HeightInPieces:uint)
		{
			trace("JigsawPuzzleGrid(WidthInPieces = " + WidthInPieces + 
				"HeightInPieces = + " + HeightInPieces + ")");
			
			_widthInPieces = WidthInPieces;
			_heightInPieces = HeightInPieces;
			
			_pieceWidth = 96;
			_pieceHeight = 96;
			_jitterPercent = new Point(0.25, 0.25);
			
			_jitters = new Vector.<Point>();
			
			var WeightingTrials:uint = 5;
			for (var Y:uint = 0; Y < HeightInPieces + 1; Y++)
			{
				for (var X:uint = 0; X < WidthInPieces + 1; X++)
				{
					var JitterY:Number = 0;
					if ((Y > 0) && (Y < HeightInPieces))
						JitterY = weightedRandomNumber(WeightingTrials);
					var JitterX:Number = 0;
					if ((X > 0) && (X < WidthInPieces))
						JitterX = weightedRandomNumber(WeightingTrials);
					var JitterPoint:Point = new Point(JitterX, JitterY);
					_jitters.push(JitterPoint);
				}
			}
			
			_currentPoint = new Point();
		}
		
		public function get pieceWidth():uint
		{
			return _pieceWidth;
		}
		
		public function set pieceWidth(Value:uint):void
		{
			_pieceWidth = Value;
		}
		
		public function get pieceHeight():uint
		{
			return _pieceHeight;
		}
		
		public function set pieceHeight(Value:uint):void
		{
			_pieceHeight = Value;
		}
		
		public function get widthInPieces():uint
		{
			return _widthInPieces;
		}
		
		public function get heightInPieces():uint
		{
			return _heightInPieces;
		}
		
		public function getPointAt(X:uint, Y:uint):Point
		{
			var Index:uint = Y * (_widthInPieces + 1) + X;
			var Jitter:Point = _jitters[Index];
			var PointX:Number = X + Jitter.x * _jitterPercent.x;
			PointX *= _pieceWidth;
			var PointY:Number = Y + Jitter.y * _jitterPercent.y;
			PointY *= _pieceHeight;
			_currentPoint.setTo(PointX, PointY);
			
			return _currentPoint;
		}
		
		/**
		 * Generates a set of random numbers between -1.0 and 1.0 and chooses 
		 * the one closest to 0.0
		 * 
		 * @param	Trials	The number of random numbers to generate
		 * 
		 * @return	The trial that was closest to 0.0.
		 */
		private function weightedRandomNumber(Trials:uint = 1):Number
		{
			var WeightedRandomNumber:Number = 2 * Math.random() - 0.5;
			if (Trials == 1)
				return WeightedRandomNumber;
			
			for (var i:uint = 1; i < Trials; i++)
			{
				var NextRandomNumber:Number = 2 * Math.random() - 0.5;
				if (Math.abs(NextRandomNumber) < Math.abs(WeightedRandomNumber))
					WeightedRandomNumber = NextRandomNumber;
			}
			
			return WeightedRandomNumber;
		}
	}
}