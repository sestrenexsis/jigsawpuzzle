{
    "puzzles" :
    [
        {
            "name"   : "Default"
        ,   "width"  : 653
        ,   "height" : 476
        ,   "pieces" :
            [
                {
                    "file_location" : "Default/000.png"
                ,   "x"             : 0
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "Default/001.png"
                ,   "x"             : 150
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "Default/002.png"
                ,   "x"             : 425
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 2
                }
            ,   {
                    "file_location" : "Default/003.png"
                ,   "x"             : 0
                ,   "y"             : 228
                ,   "row"           : 1
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "Default/004.png"
                ,   "x"             : 209
                ,   "y"             : 164
                ,   "row"           : 1
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "Default/005.png"
                ,   "x"             : 367
                ,   "y"             : 228
                ,   "row"           : 1
                ,   "column"        : 3
                }
            ]
        ,   "connections" :
            [
                {
                    "first_index"  : 0
                ,   "second_index" : 1
                }
            ,   {
                    "first_index"  : 1
                ,   "second_index" : 2
                }
            ,   {
                    "first_index"  : 0
                ,   "second_index" : 3
                }
            ,   {
                    "first_index"  : 3
                ,   "second_index" : 4
                }
            ,   {
                    "first_index"  : 1
                ,   "second_index" : 4
                }
            ,   {
                    "first_index"  : 4
                ,   "second_index" : 5
                }
            ,   {
                    "first_index"  : 2
                ,   "second_index" : 5
                }
            ]
        }
    ,   {
            "name"   : "Pixel Art"
        ,   "width"  : 253
        ,   "height" : 253
        ,   "pieces" :
            [
                {
                    "file_location" : "pixel_art/000.png"
                ,   "x"             : 0
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "pixel_art/001.png"
                ,   "x"             : 42
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "pixel_art/002.png"
                ,   "x"             : 126
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 2
                }
            ,   {
                    "file_location" : "pixel_art/003.png"
                ,   "x"             : 168
                ,   "y"             : 0
                ,   "row"           : 0
                ,   "column"        : 3
                }
            ,   {
                    "file_location" : "pixel_art/004.png"
                ,   "x"             : 0
                ,   "y"             : 63
                ,   "row"           : 1
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "pixel_art/005.png"
                ,   "x"             : 63
                ,   "y"             : 42
                ,   "row"           : 1
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "pixel_art/006.png"
                ,   "x"             : 105
                ,   "y"             : 63
                ,   "row"           : 1
                ,   "column"        : 2
                }
            ,   {
                    "file_location" : "pixel_art/007.png"
                ,   "x"             : 189
                ,   "y"             : 42
                ,   "row"           : 1
                ,   "column"        : 3
                }
            ,   {
                    "file_location" : "pixel_art/008.png"
                ,   "x"             : 0
                ,   "y"             : 105
                ,   "row"           : 2
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "pixel_art/009.png"
                ,   "x"             : 42
                ,   "y"             : 126
                ,   "row"           : 2
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "pixel_art/010.png"
                ,   "x"             : 126
                ,   "y"             : 105
                ,   "row"           : 2
                ,   "column"        : 2
                }
            ,   {
                    "file_location" : "pixel_art/011.png"
                ,   "x"             : 168
                ,   "y"             : 126
                ,   "row"           : 2
                ,   "column"        : 3
                }
            ,   {
                    "file_location" : "pixel_art/012.png"
                ,   "x"             : 0
                ,   "y"             : 189
                ,   "row"           : 3
                ,   "column"        : 0
                }
            ,   {
                    "file_location" : "pixel_art/013.png"
                ,   "x"             : 63
                ,   "y"             : 168
                ,   "row"           : 3
                ,   "column"        : 1
                }
            ,   {
                    "file_location" : "pixel_art/014.png"
                ,   "x"             : 105
                ,   "y"             : 189
                ,   "row"           : 3
                ,   "column"        : 2
                }
            ,   {
                    "file_location" : "pixel_art/015.png"
                ,   "x"             : 189
                ,   "y"             : 168
                ,   "row"           : 3
                ,   "column"        : 3
                }
            ]
        ,   "connections" :
            [
                {
                    "first_index"  : 0
                ,   "second_index" : 1
                }
            ,   {
                    "first_index"  : 1
                ,   "second_index" : 2
                }
            ,   {
                    "first_index"  : 2
                ,   "second_index" : 3
                }
            ,   {
                    "first_index"  : 0
                ,   "second_index" : 4
                }
            ,   {
                    "first_index"  : 4
                ,   "second_index" : 5
                }
            ,   {
                    "first_index"  : 1
                ,   "second_index" : 5
                }
            ,   {
                    "first_index"  : 2
                ,   "second_index" : 6
                }
            ,   {
                    "first_index"  : 5
                ,   "second_index" : 6
                }
            ,   {
                    "first_index"  : 6
                ,   "second_index" : 7
                }
            ,   {
                    "first_index"  : 3
                ,   "second_index" : 7
                }
            ,   {
                    "first_index"  : 4
                ,   "second_index" : 8
                }
            ,   {
                    "first_index"  : 8
                ,   "second_index" : 9
                }
            ,   {
                    "first_index"  : 5
                ,   "second_index" : 9
                }
            ,   {
                    "first_index"  : 9
                ,   "second_index" : 10
                }
            ,   {
                    "first_index"  : 10
                ,   "second_index" : 6
                }
            ,   {
                    "first_index"  : 10
                ,   "second_index" : 11
                }
            ,   {
                    "first_index"  : 11
                ,   "second_index" : 7
                }
            ,   {
                    "first_index"  : 8
                ,   "second_index" : 12
                }
            ,   {
                    "first_index"  : 12
                ,   "second_index" : 13
                }
            ,   {
                    "first_index"  : 13
                ,   "second_index" : 9
                }
            ,   {
                    "first_index"  : 13
                ,   "second_index" : 14
                }
            ,   {
                    "first_index"  : 14
                ,   "second_index" : 10
                }
            ,   {
                    "first_index"  : 14
                ,   "second_index" : 15
                }
            ,   {
                    "first_index"  : 15
                ,   "second_index" : 11
                }
            ]
        }
    ]
}